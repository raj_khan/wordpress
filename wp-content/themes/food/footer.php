
<div class="footer-part">
    <div class="container">
	<div class="row">
	    <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="footer-column">
		    <div class="footer-logo">
			<a href="index.html"><img src="<?php echo get_template_directory_uri(); ?>/images//footer-logo.png" alt=""></a>
		    </div>
		    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
		    <div class="social">
			<ul>
			    <li><a href="javascript:;"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
			    <li><a href="javascript:;"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
			    <li><a href="javascript:;"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
			    <li><a href="javascript:;"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			</ul>
		    </div> <!-- social -->
                </div> <!-- footer-column -->
	    </div>
	    <div class="col-md-4 col-sm-3 col-xs-12">
                <div class="footer-column">
		    <h3>Contact info</h3>
		    <div class="address-outer">
			<div class="address-blog">
			    <h6>Address</h6>
			    <p>No 13, Emerald Hills, California, USA</p>
			</div> <!-- address-blog -->
			<div class="address-blog">
			    <h6>Phone</h6>
			    <a href="javascript:;">(+800) 123 456 781</a>
			    <a href="javascript:;">(+800) 888 999 221</a>
			</div> <!-- address-blog -->
			<div class="address-blog">
			    <h6>Email</h6>
			    <a href="mailto:galliasoft@yahoo.com">galliasoft@yahoo.com</a>
			</div> <!-- address-blog -->
		    </div> <!-- address-outer -->
                </div> <!-- footer-column -->
	    </div>
	    <div class="col-md-4 col-sm-5 col-xs-12">
                <div class="footer-column footer-column-form">
		    <form class="form-footer" name="contact-form" method="post" action="http://madlenedesign.com/HTML/Food-Truck/functions.php">
			<h3>Contact us</h3>
			<div class="alert-container"></div>
			<div class="form-field">
			    <input type="text" name="full_name" placeholder="* Name" required disabled>
			</div> <!-- form-field -->
			<div class="form-field">
			    <input type="email" name="email_address" placeholder="* Email" required disabled>
			</div> <!-- form-field -->
			<div class="form-field">
			    <textarea name="message" placeholder="* comment" required disabled></textarea>
			</div> <!-- form-field -->
			<div class="form-field">
			    <button type="submit" class="btn-black send_message" disabled>Send message</button>
			</div> <!-- form-field -->
		    </form>
                </div> <!-- footer-column -->
	    </div>
	</div>
    </div>
    <div class="footer-bottom">
	<div class="container">
	    <p>© Food Truck. All rights reserved. Design by Galliasoft</p>
	</div>
    </div>
</div> <!-- footer-part -->
</div> <!-- wrapper --> 
<a href="javascript:;" class="top-arrow"></a>


<?php wp_footer(); ?>
</body>


</html>