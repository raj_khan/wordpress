<!DOCTYPE html>
<html  <?php language_attributes(); ?> class="no-js" >


    <head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title('|', true, 'right'); ?></title>

	<link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 


	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<?php wp_head(); ?>
    </head>
    <body>
	<!-- Page pre loader -->
	<!--      <div id="pre-loader">
		  <div class="loader-holder">
		      <div class="frame">
			  <img src="<?php echo get_template_directory_uri(); ?>/images//Preloader.gif" alt="Food Truck"/>                
		      </div>
		  </div>
	      </div>-->
	<div class="wrapper">
	    <div class="header-part">
		<div class="container">
		    <div class="menu-part-outer">

			<div class="menu-part">
			    <?php
			    wp_nav_menu(array(
				'theme_location' => 'main_manu',
				'menu_class' => 'menu-part',
				'fallback_cb' => 'food_theme_default_menu'
			    ));
			    ?>

<!--			    <ul>
				<li class="active"><a href="index.html" title="Home">Home</a></li>
				<li class="has-child"><a href="menu.html" title="Menu">Menu</a></li>
				<li><a href="truck-locator.html" title="Truck locator">Truck locator</a></li>
				<li class="logo"><a href="index.html"></a></li>
				<li class="has-child">
				    <a href="event.html" title="events">events<span class="caret"></span></a>
				    <ul class="sub-menu">
					<li><a href="event-detail.html">event detail</a></li>
				    </ul>
				</li>
				<li class="has-child">
				    <a href="news.html" title="News">News <span class="caret"></span></a>
				    <ul class="sub-menu">
					<li><a href="post-detail.html">News Detail</a></li>
				    </ul>
				</li>
				<li><a href="contact.html" title="contact">contact</a></li>
			    </ul>-->
			</div> <!-- menu-part -->
			<div class="logo-header">
			    <a href="index.html"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-logo.png" alt=""></a>
			</div> <!-- logo-header -->
			<div id="page">
			    <div class="header">
				<a href="#menu"></a>
			    </div>
			    <nav id="menu">
				<ul>
				    <li><a href="index.html" title="Home">Home</a></li>
				    <li><a href="menu.html" title="Menu">Menu</a></li>
				    <li><a href="truck-locator.html" title="Truck locator">Truck locator</a></li>
				    <li>
					<a href="event.html" title="events">events</a>
					<ul>
					    <li><a href="event-detail.html">event detail</a></li>
					</ul>
				    </li>
				    <li>
					<a href="news.html" title="News">News</a>
					<ul>
					    <li><a href="post-detail.html">News Detail</a></li>
					</ul>
				    </li>
				    <li><a href="contact.html" title="contact">contact</a></li>
				</ul>
			    </nav>
			</div>
		    </div> <!-- menu-part-outer -->
		</div>  <!-- container -->
	    </div> 