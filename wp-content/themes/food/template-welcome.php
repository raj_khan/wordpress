
0.
    
    
    
    
    
    
    
    
    
    
    
    
    <?php
get_header();
/*
  Template Name:  Welcome Template
 */
?>
<!-- header-part -->


<section class="slider-container">
    <div class="slider-outer">
	<div class="tp-banner-container">
	    <div class="tp-banner">
		<ul>


		    <?php
		    global $post;
		    $args = array('posts_per_page' => 5, 'post_type' => 'slide');
		    $myposts = get_posts($args);
		    foreach ($myposts as $post) : setup_postdata($post);
		    ?>
		    <?php
		    $slider_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'slider-image');
		    $slide_thumb = get_post_meta($post->ID, 'slide_thumb', true);
		    $slide_link = get_post_meta($post->ID, 'slide_link', true);
		    ?>


		    <!--Single Slider-->
		    <li data-transition="fade" data-slotamount="7" data-masterspeed="1000" data-thumb="<?php echo $slide_thumb; ?>" data-delay="5000"  data-saveperformance="on"  data-title="Slide">
			<!-- MAIN IMAGE -->
			<img src="<?php echo $slider_image[0]; ?>"  alt="slider" data-lazyload="<?php echo $slider_image[0]; ?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
			<div class="tp-caption very_large_text"
			     data-x="center"
			     data-y="center"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6">
				 <?php the_title(); ?>
			</div>
			<div class="tp-caption large_text"
			     data-x="center"
			     data-y="405"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6">
				 <?php the_excerpt(); ?>
			</div>
			<?php if ($slide_link) : ?>
			<div class="tp-caption btn-menu-top"
			     data-x="400"
			     data-y="465"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6"><a href="<?php echo $slide_link; ?>" class="btn-black">Check menu</a>
			</div>
			<?php endif; ?>

		    </li>

		    <?php endforeach; ?>
		</ul>
		<div class="tp-bannertimer"></div>
	    </div>
	</div>
    </div> <!-- slider-outer -->
</section> <!-- slider-container -->



<div class="main-part">
    <section class="shedule-container">
	<div class="shedule-part">
	    <div class="container">
                <div class="shedule-left">
		    <img src="<?php echo get_template_directory_uri(); ?>/images/shedule-left.jpg" alt="">
                </div>
                <div class="shedule-right">
		    <img src="<?php echo get_template_directory_uri(); ?>/images/shedule-bg.jpg" alt="">
		    <div class="shedule-content">
			<h2>Our Shedule</h2>


			<?php
			global $post;
			$args = array('posts_per_page' => 2, 'post_type' => 'shedule');
			$myposts = get_posts($args);
			foreach ($myposts as $post) : setup_postdata($post);
			?>
			<?php
			$slider_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'slider-image');
			$shedule_offer = get_post_meta($post->ID, 'shedule_offer', true);
			$glass_tumb = get_post_meta($post->ID, 'glass_tumb', true);
			
			?>

			<h3><?php the_title(); ?></h3>
			<div class="shedule-inside"> <?php the_excerpt(); ?></div>
			<?php endforeach; ?>
			
			
			<div class="free-juice">
			    <p><?php echo $shedule_offer; ?></p>
			</div>
			<div class="glass">
			    <img src="<?php echo $glass_tumb; ?>" alt="">
			</div>
		    </div> <!-- shedule-content -->
                </div>
	    </div> <!-- container -->
	</div> <!-- shedule-part -->
    </section> <!-- shedule-container -->




    <section class="gallery-container">
	<div class="container">
	    <div class="heading wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
                <div class="row">
		    <div class="text-center col-sm-12">
			<h2>Our gallery</h2>
		    </div>
                </div> 
	    </div> <!-- heading -->
	</div>
	<div class="gallery-outer"> 
	    <div class="col-md-6 col-sm-6 col-xs-12 left-gallery" data-wow-duration="1000ms" data-wow-delay="400ms">

		<?php
		global $post;
		$args = array('posts_per_page' => 9, 'post_type' => 'gallery');
		$myposts = get_posts($args);
		foreach ($myposts as $post) : setup_postdata($post);
		?>
		<?php
		$gallery_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'gallery-image');
		?>
                <div class="col-md-4 col-sm-4 col-xs-4 gallery-item">
		    <a href="javascript:;"><img src="<?php echo $gallery_image[0]; ?>" alt=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>

		<?php endforeach; ?>
	    </div>
	    <div class="col-md-6 col-sm-6 col-xs-12 right-gallery wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
                <div class="gallery-block-right gallery clearfix">
		    <a href="<?php echo $gallery_image[0]; ?>"><img src="<?php echo $gallery_image[0]; ?>" alt=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
	    </div>
	</div> <!-- gallery-outer -->
    </section> <!-- gallery-container -->



    <section class="product-container">
      <!-- <div class="tomato"><img src="<?php echo get_template_directory_uri(); ?>/images/tomato.png" alt=""></div>
      <div class="mashroom"><img src="<?php echo get_template_directory_uri(); ?>/images/mashroom.png" alt=""></div>
      <div class="pie-larg"><img src="<?php echo get_template_directory_uri(); ?>/images/pea-larg.png" alt=""></div> -->
	<div class="container">
	    <div class="heading wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
                <div class="row">
		    <div class="text-center col-sm-12">
			<h2>Check our dishes</h2>
		    </div>
                </div> 
	    </div> <!-- heading -->
	    <div class="dishes-container">
                <div class="row">
		    <?php
		    global $post;
		    $args = array('posts_per_page' => 6, 'post_type' => 'dish', 'orderby' => 'menu_order', 'order' => 'ASC');
		    $myposts = get_posts($args);
		    foreach ($myposts as $post) : setup_postdata($post);
		    ?>
		    <?php
		    $dish_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'dish-image');
		    $dish_price = get_post_meta($post->ID, 'dish_price', true);
		    $dish_link = get_post_meta($post->ID, 'dish_link', true);
		    ?>


		    <!--Single Dish Images-->
		    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
			<div class="dishes-list">
			    <div class="dishes-img">
				<a href="javascript:;">
				    <img src="<?php echo $dish_image[0]; ?>" alt="">
				    <div class="dishes_price">
					<div class="dishes-price-content">
					    <div class="deshies-doll"><?php echo $dish_price; ?></div>
					</div> <!-- dishes-price-content -->
				    </div> <!-- dishes_price -->
				</a>
			    </div> <!-- dishes-img -->
			    <div class="dishes-content">
				<h3><a href="<?php echo $dish_link; ?>"><?php the_title(); ?></a></h3>
				<p><?php the_excerpt(); ?></p>
				<a href="<?php echo $dish_link; ?>" class="btn-black">New</a>
			    </div> <!-- dishes-content -->
			</div> <!-- dishes-list -->
		    </div>

		    <?php endforeach; ?>

                </div>
	    </div> <!-- dishes-container -->
	</div>
    </section>
    
    
    
    
    <section class="our-client-container">
	<div class="container"> 
	    <div class="heading wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
                <div class="row">
		    <div class="text-center col-sm-12">
			<h2>What our clients say</h2>
		    </div>
                </div> 
	    </div> <!-- heading -->
	    <div class="our-client">
                <div class="col-md-4 col-sm-5 col-xs-12">
		    <div class="client-content wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
			<p><span class="quort">“</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			<b>Alicia</b>
		    </div> <!-- client-content -->
		    <div class="client-content wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
			<p><span class="quort">“</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			<b>Marissa</b>
		    </div> <!-- client-content -->
		    <div class="client-content wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
			<p><span class="quort">“</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			<b>Tom</b>
		    </div> <!-- client-content -->
                </div>
                <div class="col-md-8 col-sm-7 col-xs-12 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
		    <img src="<?php echo get_template_directory_uri(); ?>/images/client.jpg" alt="">
                </div>
	    </div> <!-- our-client -->
	</div>
    </section> <!-- our-client-container -->
    <section class="find-container">
	<div class="find-where-you">
	    <div class="food1">
                <img src="<?php echo get_template_directory_uri(); ?>/images/food1.png" alt="">
	    </div>
	    <div class="container">
                <div class="row">
		    <div class="col-md-6 col-sm-6 col-xs-12 find-location-left">
			<h2>Where you can find us</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
		    </div>
		    <div class="col-md-6 col-sm-6 col-xs12 find-location-right">
			<div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
			<div class="location-content"><p>No 13, Emerald Hills, California, USA <br> No 113, Farm Hill Blvd, California, USA <br> No 97, Huddart Park, California, USA <br> No 13, Alameda de las Pulgas, California, USA</p></div>
		    </div>
                </div>
	    </div>
	    <div class="food2">
                <img src="<?php echo get_template_directory_uri(); ?>/images/food2.png" alt="">
	    </div>
	</div> <!-- find-where-you -->
    </section> <!-- find-container -->
    <div class="map-outer">
	<div id="truck-locator"></div>
    </div>
</div> <!-- main-part -->



<?php get_footer(); ?>