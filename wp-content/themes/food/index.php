<?php get_header(); ?>
<!-- header-part -->


<section class="slider-container">
    <div class="slider-outer">
	<div class="tp-banner-container">
	    <div class="tp-banner">
		<ul>
		    <li data-transition="fade" data-slotamount="7" data-masterspeed="1000" data-thumb="<?php echo get_template_directory_uri(); ?>/images/thumb1.jpg" data-delay="5000"  data-saveperformance="on"  data-title="Slide">
			<!-- MAIN IMAGE -->
			<img src="<?php echo get_template_directory_uri(); ?>/images/dummy.png"  alt="slider" data-lazyload="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
			<div class="tp-caption very_large_text"
			     data-x="center"
			     data-y="center"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6">WELCOME to FOOD TRUCK
			</div>
			<div class="tp-caption large_text"
			     data-x="center"
			     data-y="405"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6">Check now our menu and see the chedule
			</div>
			<div class="tp-caption btn-menu-top"
			     data-x="400"
			     data-y="465"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6"><a href="javascript:;" class="btn-black">Check menu</a>
			</div>
			<div class="tp-caption btn-menu-bottom"
			     data-x="570"
			     data-y="465"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6"><a href="javascript:;" class="btn-black">See schedule</a>
			</div> 
		    </li>
		    <li data-transition="zoomin" data-slotamount="7" data-masterspeed="1500" data-thumb="<?php echo get_template_directory_uri(); ?>/images/thumb1.jpg" data-delay="5000"  data-saveperformance="on"  data-title="Slide">
			<!-- MAIN IMAGE -->
			<img src="<?php echo get_template_directory_uri(); ?>/images/dummy.png"  alt="slider" data-lazyload="<?php echo get_template_directory_uri(); ?>/images/slider2.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
			<div class="tp-caption very_large_text"
			     data-x="center"
			     data-y="center"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6">WELCOME to FOOD TRUCK
			</div>
			<div class="tp-caption large_text"
			     data-x="center"
			     data-y="405"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6">Check now our menu and see the chedule
			</div>
			<div class="tp-caption btn-menu-top"
			     data-x="400"
			     data-y="465"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6"><a href="javascript:;" class="btn-black">Check menu</a>
			</div>
			<div class="tp-caption btn-menu-bottom"
			     data-x="570"
			     data-y="465"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6"><a href="javascript:;" class="btn-black">See schedule</a>
			</div> 
		    </li>
		    <li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-thumb="<?php echo get_template_directory_uri(); ?>/images/thumb1.jpg" data-delay="5000"  data-saveperformance="on"  data-title="Slide">
			<!-- MAIN IMAGE -->
			<img src="<?php echo get_template_directory_uri(); ?>/images/dummy.png"  alt="slider" data-lazyload="<?php echo get_template_directory_uri(); ?>/images/slider3.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
			<div class="tp-caption very_large_text"
			     data-x="center"
			     data-y="center"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6">WELCOME to FOOD TRUCK
			</div>
			<div class="tp-caption large_text"
			     data-x="center"
			     data-y="405"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6">Check now our menu and see the chedule
			</div>
			<div class="tp-caption btn-menu-top"
			     data-x="400"
			     data-y="465"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6"><a href="javascript:;" class="btn-black">Check menu</a>
			</div>
			<div class="tp-caption btn-menu-bottom"
			     data-x="570"
			     data-y="465"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6"><a href="javascript:;" class="btn-black">See schedule</a>
			</div> 
		    </li>
		    <li data-transition="zoomin" data-slotamount="7" data-masterspeed="2500" data-thumb="<?php echo get_template_directory_uri(); ?>/images/thumb1.jpg" data-delay="5000"  data-saveperformance="on"  data-title="Slide">
			<!-- MAIN IMAGE -->
			<img src="<?php echo get_template_directory_uri(); ?>/images/dummy.png"  alt="slider" data-lazyload="<?php echo get_template_directory_uri(); ?>/images/slider4.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
			<div class="tp-caption very_large_text"
			     data-x="center"
			     data-y="center"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6">WELCOME to FOOD TRUCK
			</div>
			<div class="tp-caption large_text"
			     data-x="center"
			     data-y="405"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6">Check now our menu and see the chedule
			</div>
			<div class="tp-caption btn-menu-top"
			     data-x="400"
			     data-y="465"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6"><a href="javascript:;" class="btn-black">Check menu</a>
			</div>
			<div class="tp-caption btn-menu-bottom"
			     data-x="570"
			     data-y="465"
			     data-speed="800"
			     data-start="3300"
			     data-easing="Power4.easeOut"
			     data-endspeed="300"
			     data-endeasing="Power1.easeIn"
			     data-captionhidden="off"
			     style="z-index: 6"><a href="javascript:;" class="btn-black">See schedule</a>
			</div> 
		    </li>
		</ul>
		<div class="tp-bannertimer"></div>
	    </div>
	</div>
    </div> <!-- slider-outer -->
</section> <!-- slider-container -->
<div class="main-part">
    <section class="shedule-container">
	<div class="shedule-part">
	    <div class="container">
                <div class="shedule-left">
		    <img src="<?php echo get_template_directory_uri(); ?>/images/shedule-left.jpg" alt="">
                </div>
                <div class="shedule-right">
		    <img src="<?php echo get_template_directory_uri(); ?>/images/shedule-bg.jpg" alt="">
		    <div class="shedule-content">
			<h2>Our Shedule</h2>
			<h3>Food truck</h3>
			<div class="shedule-inside">
			    <h6>Monday-Friday: 800<sup>00</sup> - 2000<sup>00</sup></h6>
			    <h6>Saturday: 900<sup>00</sup> - 1700<sup>00</sup></h6>
			    <h6>Sunday: closed</h6>
			</div>
			<h3>Catering</h3>
			<div class="shedule-inside">
			    <h6>Monday-Friday: 800<sup>00</sup> - 2400<sup>00</sup></h6>
			    <h6>Saturday: 900<sup>00</sup> - 2200<sup>00</sup></h6>
			    <h6>Sunday: closed</h6>
			</div>
			<div class="free-juice">
			    <p>*Daily first 5 clients receive a free juice</p>
			</div>
			<div class="glass">
			    <img src="<?php echo get_template_directory_uri(); ?>/images/glass.png" alt="">
			</div>
		    </div> <!-- shedule-content -->
                </div>
	    </div> <!-- container -->
	</div> <!-- shedule-part -->
    </section> <!-- shedule-container -->
    <section class="gallery-container">
	<div class="container">
	    <div class="heading wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
                <div class="row">
		    <div class="text-center col-sm-12">
			<h2>Our gallery</h2>
		    </div>
                </div> 
	    </div> <!-- heading -->
	</div>
	<div class="gallery-outer"> 
	    <div class="col-md-6 col-sm-6 col-xs-12 left-gallery" data-wow-duration="1000ms" data-wow-delay="400ms">
                <div class="col-md-4 col-sm-4 col-xs-4 gallery-item">
		    <a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery-big-right1.jpg" alt=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 gallery-item">
		    <a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery-big-right2.jpg" alt=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 gallery-item">
		    <a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery-big-right3.jpg" alt=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 gallery-item">
		    <a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery-big-right4.jpg" alt=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 gallery-item">
		    <a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery-big-right5.jpg" alt=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 gallery-item">
		    <a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery-big-right6.jpg" alt=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 gallery-item">
		    <a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery-big-right7.jpg" alt=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 gallery-item">
		    <a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery-big-right8.jpg" alt=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 gallery-item">
		    <a href="javascript:;"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery-big-right9.jpg" alt=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
	    </div>
	    <div class="col-md-6 col-sm-6 col-xs-12 right-gallery wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
                <div class="gallery-block-right gallery clearfix">
		    <a href="<?php echo get_template_directory_uri(); ?>/images/gallery-big-right1.jpg" rel="prettyPhoto[gallery1]"><img src="<?php echo get_template_directory_uri(); ?>/images/gallery-big-right1.jpg" alt=""><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
	    </div>
	</div> <!-- gallery-outer -->
    </section> <!-- gallery-container -->
    <section class="product-container">
      <!-- <div class="tomato"><img src="<?php echo get_template_directory_uri(); ?>/images/tomato.png" alt=""></div>
      <div class="mashroom"><img src="<?php echo get_template_directory_uri(); ?>/images/mashroom.png" alt=""></div>
      <div class="pie-larg"><img src="<?php echo get_template_directory_uri(); ?>/images/pea-larg.png" alt=""></div> -->
	<div class="container">
	    <div class="heading wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
                <div class="row">
		    <div class="text-center col-sm-12">
			<h2>Check our dishes</h2>
		    </div>
                </div> 
	    </div> <!-- heading -->
	    <div class="dishes-container">
                <div class="row">
		    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
			<div class="dishes-list">
			    <div class="dishes-img">
				<a href="javascript:;">
				    <img src="<?php echo get_template_directory_uri(); ?>/images/dishes-item1.jpg" alt="">
				    <div class="dishes_price">
					<div class="dishes-price-content">
					    <div class="deshies-doll">$15</div>
					</div> <!-- dishes-price-content -->
				    </div> <!-- dishes_price -->
				</a>
			    </div> <!-- dishes-img -->
			    <div class="dishes-content">
				<h3><a href="javascript:;">Big Bur</a></h3>
				<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla</p>
				<a href="javascript:;" class="btn-black">New</a>
			    </div> <!-- dishes-content -->
			</div> <!-- dishes-list -->
		    </div>
		    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
			<div class="dishes-list">
			    <div class="dishes-img">
				<a href="javascript:;">
				    <img src="<?php echo get_template_directory_uri(); ?>/images/dishes-item2.jpg" alt="">
				    <div class="dishes_price">
					<div class="dishes-price-content">
					    <div class="deshies-doll">$19</div>
					</div> <!-- dishes-price-content -->
				    </div> <!-- dishes_price -->
				</a>
			    </div> <!-- dishes-img -->
			    <div class="dishes-content">
				<h3><a href="javascript:;">Quesadillas</a></h3>
				<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla</p>
				<a href="javascript:;" class="btn-black">Chef Selection</a>
			    </div> <!-- dishes-content -->
			</div> <!-- dishes-list -->
		    </div>
		    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
			<div class="dishes-list">
			    <div class="dishes-img">
				<a href="javascript:;">
				    <img src="<?php echo get_template_directory_uri(); ?>/images/dishes-item3.jpg" alt="">
				    <div class="dishes_price">
					<div class="dishes-price-content">
					    <div class="deshies-doll">$21</div>
					</div> <!-- dishes-price-content -->
				    </div> <!-- dishes_price -->
				</a>
			    </div> <!-- dishes-img -->
			    <div class="dishes-content">
				<h3><a href="javascript:;">Hamburger</a></h3>
				<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla</p>
				<a href="javascript:;" class="btn-black">Recommended</a>
			    </div> <!-- dishes-content -->
			</div> <!-- dishes-list -->
		    </div>
		    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
			<div class="dishes-list">
			    <div class="dishes-img">
				<a href="javascript:;">
				    <img src="<?php echo get_template_directory_uri(); ?>/images/dishes-item4.jpg" alt="">
				    <div class="dishes_price">
					<div class="dishes-price-content">
					    <div class="deshies-doll">$15</div>
					</div> <!-- dishes-price-content -->
				    </div> <!-- dishes_price -->
				</a>
			    </div> <!-- dishes-img -->
			    <div class="dishes-content">
				<h3><a href="javascript:;">Fruits salad</a></h3>
				<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla</p>
				<a href="javascript:;" class="btn-black">Recommended</a>
			    </div> <!-- dishes-content -->
			</div> <!-- dishes-list -->
		    </div>
		    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
			<div class="dishes-list">
			    <div class="dishes-img">
				<a href="javascript:;">
				    <img src="<?php echo get_template_directory_uri(); ?>/images/dishes-item5.jpg" alt="">
				    <div class="dishes_price">
					<div class="dishes-price-content">
					    <div class="deshies-doll"><del>$25</del>$21</div>
					</div> <!-- dishes-price-content -->
				    </div> <!-- dishes_price -->
				</a>
			    </div> <!-- dishes-img -->
			    <div class="dishes-content">
				<h3><a href="javascript:;">Hamburger</a></h3>
				<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla</p>
				<a href="javascript:;" class="btn-black">New</a>
			    </div> <!-- dishes-content -->
			</div> <!-- dishes-list -->
		    </div>
		    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
			<div class="dishes-list">
			    <div class="dishes-img">
				<a href="javascript:;">
				    <img src="<?php echo get_template_directory_uri(); ?>/images/dishes-item6.jpg" alt="">
				    <div class="dishes_price">
					<div class="dishes-price-content">
					    <div class="deshies-doll">$21</div>
					</div> <!-- dishes-price-content -->
				    </div> <!-- dishes_price -->
				</a>
			    </div> <!-- dishes-img -->
			    <div class="dishes-content">
				<h3><a href="javascript:;">Salad</a></h3>
				<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla</p>
				<a href="javascript:;" class="btn-black">Chef Selection</a>
			    </div> <!-- dishes-content -->
			</div> <!-- dishes-list -->
		    </div>
                </div>
	    </div> <!-- dishes-container -->
	</div>
    </section>
    <section class="our-client-container">
	<div class="container"> 
	    <div class="heading wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
                <div class="row">
		    <div class="text-center col-sm-12">
			<h2>What our clients say</h2>
		    </div>
                </div> 
	    </div> <!-- heading -->
	    <div class="our-client">
                <div class="col-md-4 col-sm-5 col-xs-12">
		    <div class="client-content wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
			<p><span class="quort">“</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			<b>Alicia</b>
		    </div> <!-- client-content -->
		    <div class="client-content wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
			<p><span class="quort">“</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			<b>Marissa</b>
		    </div> <!-- client-content -->
		    <div class="client-content wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
			<p><span class="quort">“</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			<b>Tom</b>
		    </div> <!-- client-content -->
                </div>
                <div class="col-md-8 col-sm-7 col-xs-12 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
		    <img src="<?php echo get_template_directory_uri(); ?>/images/client.jpg" alt="">
                </div>
	    </div> <!-- our-client -->
	</div>
    </section> <!-- our-client-container -->
    <section class="find-container">
	<div class="find-where-you">
	    <div class="food1">
                <img src="<?php echo get_template_directory_uri(); ?>/images/food1.png" alt="">
	    </div>
	    <div class="container">
                <div class="row">
		    <div class="col-md-6 col-sm-6 col-xs-12 find-location-left">
			<h2>Where you can find us</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
		    </div>
		    <div class="col-md-6 col-sm-6 col-xs12 find-location-right">
			<div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
			<div class="location-content"><p>No 13, Emerald Hills, California, USA <br> No 113, Farm Hill Blvd, California, USA <br> No 97, Huddart Park, California, USA <br> No 13, Alameda de las Pulgas, California, USA</p></div>
		    </div>
                </div>
	    </div>
	    <div class="food2">
                <img src="<?php echo get_template_directory_uri(); ?>/images/food2.png" alt="">
	    </div>
	</div> <!-- find-where-you -->
    </section> <!-- find-container -->
    <div class="map-outer">
	<div id="truck-locator"></div>
    </div>
</div> <!-- main-part -->



<?php get_footer(); ?>