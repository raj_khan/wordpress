<?php


//Including food theme all css & js files
function food_theme_files(){
    
    
// ================Including All CSS Files   ==================
// 
//    Font_Awesome
    wp_register_style ('font-awesome', get_template_directory_uri() . '/css/font-awesome.css', array(), '4.3.0', 'all');
    
//    bootstrap
    wp_register_style ('bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '3.3.4', 'all');
    
//    swiper
    wp_register_style ('swiper', get_template_directory_uri() . '/css/swiper.css', array(), '3.1.2', 'all');
    
//    extralayers
    wp_register_style ('extralayers', get_template_directory_uri() . '/css/extralayers.css', array(), '1.0', 'all');
    
//    settings
    wp_register_style ('settings', get_template_directory_uri() . '/css/settings.css', array(), '1.4.5', 'all');
    
//    animate.min
    wp_register_style ('animate.min', get_template_directory_uri() . '/css/animate.min.css', array(), '1.0', 'all');
    
//    prettyPhoto
    wp_register_style ('prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css', array(), '3.1.6', 'all');
    
//    demo
    wp_register_style ('demo', get_template_directory_uri() . '/css/demo.css', array(), '1.0', 'all');
    
    
//    jquery.mmenu.all
    wp_register_style ('jquery.mmenu.all', get_template_directory_uri() . '/css/jquery.mmenu.all.css', array(), '5.2.0', 'all');
    
    
    
//    style
    wp_register_style ('style', get_template_directory_uri() . '/css/style.css', array(), '1.0', 'all');
    
    
    
//    responsive
    wp_register_style ('responsive', get_template_directory_uri() . '/css/responsive.css', array(), '1.0', 'all');
    
    
    
    wp_enqueue_style ('font-awesome');
    wp_enqueue_style ('bootstrap');
    wp_enqueue_style ('swiper');
    wp_enqueue_style ('extralayers');
    wp_enqueue_style ('settings');
    wp_enqueue_style ('animate.min');
    wp_enqueue_style ('prettyPhoto');
    wp_enqueue_style ('demo');
    wp_enqueue_style ('jquery.mmenu.all');
    wp_enqueue_style ('style');
    wp_enqueue_style ('responsive');
    
    
    
    
// ================Including All JS Files   ==================//

//    Jquery    
    wp_enqueue_script('jquery');
//    
//    Bootstrap
    wp_enqueue_script ('bootstrap-js', get_template_directory_uri(). '/js/bootstrap.js', array('jquery'), '3.3.4', true);
    
//    swiper
    wp_enqueue_script ('swiper-js', get_template_directory_uri(). '/js/swiper.js', array('jquery'), '3.1.2', true);
    
//    jquery.themepunch.plugins.min
    wp_enqueue_script ('jquery.themepunch.plugins.min', get_template_directory_uri(). '/js/jquery.themepunch.plugins.min.js', array('jquery'), '1.0.5', true);
    
    
//    jquery.themepunch.revolution.min
    wp_enqueue_script ('jquery.themepunch.revolution.min', get_template_directory_uri(). '/js/jquery.themepunch.revolution.min.js', array('jquery'), '4.5.3', true);
    
    
//    wow.min
    wp_enqueue_script ('wow.min', get_template_directory_uri(). '/js/wow.min.js', array('jquery'), '0.1.9', true);
    
    
//    jquery.mmenu.min.all
    wp_enqueue_script ('jquery.mmenu.min.all', get_template_directory_uri(). '/js/jquery.mmenu.min.all.js', array('jquery'), '5.2.0', true);
    
    
//    jquery.prettyPhoto
    wp_enqueue_script ('jquery.prettyPhoto', get_template_directory_uri(). '/js/jquery.prettyPhoto.js', array('jquery'), '3.1.6', true);
    
    
    
//    jquery.isotope
    wp_enqueue_script ('jquery.isotope', get_template_directory_uri(). '/js/jquery.isotope.js', array('jquery'), '1.5.25', true);
    
    
    
//    script
    wp_enqueue_script ('script', get_template_directory_uri(). '/js/script.js', array('jquery'), '1.0', true);
    
}
add_action('wp_enqueue_scripts', 'food_theme_files');

