<?php

//Events Shortcodes
function events_shortcode($atts) {

    extract(shortcode_atts(array(
	'title' => '',
	'text' => ''
		    ), $atts));


    return ' 
		<div class="event-list wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInDown;">
		    <h3>' . $title . '</h3>
		    <p>' . $text . '</p>
		</div> 
	';
}

add_shortcode('events', 'events_shortcode');

//Events Shortcodes
function youtube_shortcode($atts) {

    extract(shortcode_atts(array(
	'id' => ''
		    ), $atts));


    return ' 
	<div class="container">
	<div class="row">
	<div class="col-md-10 col-md-offset-1"> 
	<div class="embed-responsive embed-responsive-16by9">
	<iframe class="embed-responsive-item" width="1263" height="708" src="https://www.youtube.com/embed/' . $id . '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div> 
	</div> 
	</div> 
	</div> 
	';
}

add_shortcode('youtube', 'youtube_shortcode');

