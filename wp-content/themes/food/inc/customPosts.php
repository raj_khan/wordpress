<?php

function food_theme_custom_posts(){
    
//    Slider
    register_post_type('slide', array(
	'label' => 'slides',
	'labels' => array(
	    'name' => 'slides',
	    'singular_name' => 'slide'
	),
	'public' => true,
	'menu_icon' => 'dashicons-images-alt',
	'supports' => array('title', 'editor', 'thumbnail', 'custom-fields', 'excerpt')
    ));
    
//    Dishes 
    register_post_type('dish', array(
	'label' => 'dishes',
	'labels' => array(
	    'name' => 'dishes',
	    'singular_name' => 'dish'
	),
	'public' => true,
	'menu_icon' => 'dashicons-format-image',
	'supports' => array('title', 'editor', 'thumbnail', 'custom-fields', 'excerpt', 'page-attributes')
    ));
    
//    Gallery 
    register_post_type('gallery', array(
	'label' => 'gallerys',
	'labels' => array(
	    'name' => 'gallerys',
	    'singular_name' => 'gallery'
	),
	'public' => true,
	'menu_icon' => 'dashicons-images-alt2',
	'supports' => array('thumbnail')
    ));
    
    
//     Shedule 
    register_post_type('shedule', array(
	'label' => 'shedules',
	'labels' => array(
	    'name' => 'shedules',
	    'singular_name' => 'shedule'
	),
	'public' => true,
	'menu_icon' => 'dashicons-analytics',
	'supports' => array('title', 'editor', 'thumbnail', 'custom-fields',)
    ));
}
add_action('init', 'food_theme_custom_posts');