<?php



function food_theme_menus() {
    register_nav_menus(array(
	'main_manu' => 'Main Menu',
	'footer_manu' => 'Footer Menu'
    ));
}

add_action('init', 'food_theme_menus');

function food_theme_default_menu() {
    echo '<div class = "menu-part">';
    if ('page' != get_option('show_on_front')) {
	echo '<ul> <li><a href= " ' . home_url() . '/">Home</a> </li>';
    }
    wp_list_pages('title_li=');
    echo '</div> </ul>';
}
