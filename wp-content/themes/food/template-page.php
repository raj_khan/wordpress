<?php get_header(); ?>

<!-- header-part -->
<section class="bread-crumb-container">
    <div class="bread-crumb-img">
	<img src="<?php echo get_template_directory_uri(); ?>/images/bread-crumb5.png" alt="">
    </div>
    <div class="bread-crumb-content">
	<div class="bread-crumb-inner">
	    <h2>Page</h2>
	    <a href="index.html">Home</a> - <a href="news.html">News</a>
	</div>
    </div> <!-- bread-crumb-content -->
</section> <!-- bread-crumb-container -->


<div class="main-part">
    <section class="event-container">
	<div class="container">
	    <div class="col-md-12">

		<?php if (have_posts()) : ?><?php while (have_posts()) : the_post(); ?>
			<h2><?php the_title(); ?></h2>
			<?php the_content(); ?>

		    <?php endwhile; ?>

		<?php else : ?>
			<h2>404 Data Not Found!</h2>

		<?php endif; ?>

	    </div> <!-- post-detail -->
	</div>
    </section> <!-- event-container -->

</div> <!-- main-part -->


<?php get_footer(); ?>