<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'food');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '![fHl?Z48XSt$_>uXll^LjlubS-v%yK2H97UD2SqISTl{#,nvWJhpgLzTax*.nEo');
define('SECURE_AUTH_KEY',  'xtJ(Q`nZ9e^{2+5-zxjD2+c[xL*X3|t0A@^tPx~s^+M$D_d&Pp@~b#i!utFMC5Q=');
define('LOGGED_IN_KEY',    'R`$hL|:Oo}/]TT)}$cM72y.sP-t8,3B{PcT3e&.#-730 )KrkCo35J$*x$laFEY]');
define('NONCE_KEY',        'v|m[x,x`X4Vx4;]^3VO7Yq#0[#bks&=/=5_Yf ecMjdSE9qUMkhMojj<Z$A,uErJ');
define('AUTH_SALT',        '3`Ir)N^Zl7@0U|;`5.*Q6(>qZ;X3e%yY`m?IwoTS: ygEa&hm @7%v?3H>v%am]5');
define('SECURE_AUTH_SALT', '>>/bmbV^s%s,rW/J=Nx`(g1GY !|rpxIE^uX:h7GtS/~46J&nk h~pwTxe8mPUv`');
define('LOGGED_IN_SALT',   '};|rcX6s[5|[yLPG43,X;vnwJysi_{0^BKq<foFSbqK<FXb6n]FseE1Q{CX_*aV ');
define('NONCE_SALT',       '^hS:T@G&v`@T(U[Ur]G?}[U;bd#eOCSpq$E/I:GXO%|aVjIu[Jt%$(m%MLLS92sB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'food_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
